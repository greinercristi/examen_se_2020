public class MyThreads extends Thread {

    @Override
    public void run() {

        for (int i = 1; i <= 10; i++) {
            System.out.println(Thread.currentThread().getName() + " - " + i);
            try {
                Thread.sleep(4000);

            } catch (InterruptedException e) {

            }
        }
    }
}


class Main {
        public static void main(String[] args) {
            MyThreads IThread1 = new MyThreads();
            MyThreads IThread2 = new MyThreads();

            IThread1.setName("IThread1");
            IThread2.setName("IThread2");

            IThread1.start();

            IThread2.start();
        }
    }



